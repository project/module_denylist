<?php

namespace Drupal\module_denylist\Exception;

/**
 * Exception class to throw when a denylisted module is being installed.
 */
class ModuleInstallationBlocked extends \Exception {}
