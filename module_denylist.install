<?php

/**
 * @file
 * Install, update and uninstall functions for the Module Denylist module.
 */

use Drupal\Core\Site\Settings;
use Drupal\module_denylist\Exception\ModuleInstallationBlocked;

/**
 * Implements hook_install().
 */
function module_denylist_install() {
  if (!\Drupal::isConfigSyncing()) {
    return;
  }

  // The hook_requirements() is not automatically called during the module
  // installation by config management, so it is necessary to call it manually.
  // The requirement is to fail the installation if denylisted modules were
  // installed before Module Denylist.
  $requirements = \Drupal::moduleHandler()->invoke('module_denylist', 'requirements', ['install']);
  if (!$requirements) {
    return;
  }

  // Hard fail installation.
  $exception_message = $requirements['denylisted_modules_discovery']['description']->render();
  throw new ModuleInstallationBlocked($exception_message);
}

/**
 * Implements hook_requirements().
 */
function module_denylist_requirements($phase) {
  if ($phase !== 'install') {
    return [];
  }

  $denylisted_modules = Settings::get('module_denylist');
  if (!$denylisted_modules) {
    return [];
  }

  $enabled_modules = array_keys(\Drupal::moduleHandler()->getModuleList());
  $denylisted_modules_enabled = array_intersect($enabled_modules, $denylisted_modules);

  if (!$denylisted_modules_enabled) {
    return [];
  }

  $requirements = [
    'denylisted_modules_discovery' => [
      'description' => t('You need to uninstall the denylisted module(s) @modules before installing Module Denylist.', ['@modules' => implode(',', $denylisted_modules_enabled)]),
      'severity' => REQUIREMENT_ERROR,
    ],
  ];

  return $requirements;
}
